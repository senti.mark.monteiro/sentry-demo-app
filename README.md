A demo app demonstrating issues setting context in a WPF app. In reference to this issue on the Sentry forum:
https://forum.sentry.io/t/user-context-not-submitting-consistently/10806

For context, this is a [Prism](prismlibrary.com) WPF application running on the .NET Framework. It is initialized from
a standard empty Prism project template.

The Sentry SDK is initialized in [App.xaml.cs](SentryDemoApp/App.xaml.cs). An exception handler is also set here to
capture application exceptions.

The user context is set in [MainWindowViewModel.cs](SentryDemoApp/ViewModels/MainWindowViewModel.cs) (**to run the
application you will need to edit this file to add your DSN string**). To demonstrate the issue, an async login operation
is simulated with `Task.Delay()` and `ConfigureAwait(false)`. To get back onto the UI thread (where the Sentry SDK
is initialized), I've used `Application.Current.Dispatcher.Invoke()`. However, updating scope in this context does
not appear to work and the user information is not submitted. Setting the context without using the dispatcher to
switch back to the UI thread also does not work, and using `ConfigureAwait(true)` does not appear to have any effect
either.
