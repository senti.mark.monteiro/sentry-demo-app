﻿using SentryDemoApp.Views;
using Prism.Ioc;
using Prism.Modularity;
using System.Windows;
using Sentry;
using System.Windows.Threading;
using System;

namespace SentryDemoApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            SentrySdk.Init(options =>
            {
                // NOTE: Enter your personal DSN string here
                options.Dsn = new Dsn(null);
                options.Debug = true;
            });

            DispatcherUnhandledException += (_, exEvent) =>
            {
                SentrySdk.CaptureException(exEvent.Exception);
                _ = SentrySdk.FlushAsync(TimeSpan.FromSeconds(1));
                exEvent.Handled = true;
            };

            base.OnStartup(e);
        }

        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}
