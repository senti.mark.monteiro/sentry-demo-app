﻿using Prism.Commands;
using Prism.Mvvm;

using Sentry;
using Sentry.Protocol;

using System;
using System.Threading.Tasks;
using System.Windows;

namespace SentryDemoApp.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        public string Title => "Prism Application";

        public DelegateCommand SetUserCommand { get; }

        public DelegateCommand ThrowExceptionCommand { get; }

        public MainWindowViewModel()
        {
            SetUserCommand = new DelegateCommand(SetUser);
            ThrowExceptionCommand = new DelegateCommand(ThrowException);
        }

        public async void SetUser()
        {
            // Simulate logging in the user async
            await Task.Delay(TimeSpan.FromMilliseconds(500)).ConfigureAwait(false);

            // Set context on the UI thread since we used ConfigureAwait(false) above
            Application.Current.Dispatcher.Invoke(() =>
            {
                SentrySdk.ConfigureScope(scope =>
                {
                    scope.User = new User
                    {
                        Id = Guid.NewGuid().ToString()
                    };
                });
            });
        }

        public void ThrowException()
        {
            throw new Exception("Test Exception");
        }
    }
}
